import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subject2 {
    public static void main(String[] args) {
        Window.getInstance();
    }
}

class Window extends JFrame {

    private static Window instance;
    private JButton button;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;

    public static Window getInstance() {
        if (instance == null) {
            instance = new Window();
        }
        return instance;
    }

    private Window() {
        initializationWindow();
    }

    private void initializationWindow() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300, 300);
        this.setLayout(new GridLayout(4, 1));
        this.setResizable(false);

        textField1 = new JTextField();
        textField2 = new JTextField();
        textField3 = new JTextField();
        textField3.setEditable(false);
        button = new JButton("Click");
        button.addActionListener(new ActionButton());

        this.add(textField1);
        this.add(textField2);
        this.add(textField3);
        this.add(button);

        this.setVisible(true);
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public JTextField getTextField3() {
        return textField3;
    }
}

class ActionButton implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        Window window = Window.getInstance();
        int length1 = window.getTextField1().getText().length();
        int length2 = window.getTextField2().getText().length();
        int s = length1 + length2;
        window.getTextField3().setText("" + s);
    }
}
