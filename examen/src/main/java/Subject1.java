public class Subject1 {
    public static void main(String[] args) {
    }
}

class A {

}

class B {
    private long t;
    private D d;
    private E e;

    public void f() {

    }

    public void metJ(A j) {

    }

}

class C {

}

class D {

}

class E extends C {
    private F f;

    public E() {
        f = new F();
    }

    public void metG(int i) {

    }
}

class F {
    public void metA() {

    }
}
